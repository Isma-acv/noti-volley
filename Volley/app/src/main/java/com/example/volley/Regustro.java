package com.example.volley;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Regustro extends AppCompatActivity {

    private RequestQueue queue;
    String usuario,contrasena;
    EditText EdtUsuario,password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regustro);

        queue = Volley.newRequestQueue(this);
        EdtUsuario = (EditText) findViewById(R.id.edtcorreo);
        password = (EditText) findViewById(R.id.edtcontrasena);


    }//oncreate



    private void obtenerDatosVolley(){
        usuario =EdtUsuario.getText().toString();
        contrasena =password.getText().toString();

        if(TextUtils.isEmpty(usuario)){
            EdtUsuario.setError("Porfavor Ingrese su Usuario");
            EdtUsuario.requestFocus();
            return;
        }
        if(TextUtils.isEmpty(contrasena)){
            password.setError("Ingrse una contraeña");
            password.requestFocus();
            return;
        }

        String url="https://notificacionupt.andocodeando.net/api/crearUsuario";

        StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object=new JSONObject(response);
                    String estado=object.getString("estado");

                    if (estado.equals("true")){
                        Toast.makeText(Regustro.this ,"Usuario regustrado con exito", Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(Regustro.this ,"no se pudo completar el registro", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Regustro.this ,"ALGO SALIO MAL"+error, Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams(){
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", usuario);
                params.put("password", contrasena);
                return params;
            }
        };
        queue.add(postRequest);

    }//metodo obtener ddatos

    public void logeo(View view){
        obtenerDatosVolley();
    }




}//clase