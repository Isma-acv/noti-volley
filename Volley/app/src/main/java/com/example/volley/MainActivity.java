package com.example.volley;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.util.HashMap;
import java.util.Map;


public class MainActivity extends AppCompatActivity {

    private RequestQueue queue;
    String usuario,contrasena;
    EditText EdtUsuario,password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        queue = Volley.newRequestQueue(this);
        EdtUsuario = (EditText) findViewById(R.id.editTextTextcorreo);
        password = (EditText) findViewById(R.id.editTextTextPassword);



    }//oncreate

    private void obtenerDatosVolley(){
        usuario =EdtUsuario.getText().toString();
        contrasena =password.getText().toString();

        if(TextUtils.isEmpty(usuario)){
            EdtUsuario.setError("Porfavor Ingrese su Usuario");
            EdtUsuario.requestFocus();
            return;
        }
        if(TextUtils.isEmpty(contrasena)){
            password.setError("Ups! se le olvida lo mas Importante");
            password.requestFocus();
            return;
        }

        String url="https://notificacionupt.andocodeando.net/api/login";

        StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object=new JSONObject(response);
                    String estado=object.getString("estado");
                    if (estado.equals("true")){
                        String token=object.getString("token");
                        Toast.makeText(MainActivity.this ,"Token: "+token, Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(MainActivity.this ,"NO EXISTE", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this ,"ALGO SALIO MAL"+error, Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams(){
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", usuario);
                params.put("password", contrasena);
                return params;
            }
        };
        queue.add(postRequest);

    }//metodo obtener ddatos

    //-- Button
    public void enviar(View view){
        obtenerDatosVolley();
    }
    public void Goregistro(View view){
        Intent IraRegistro = new Intent(MainActivity.this,Regustro.class);
        startActivity(IraRegistro);
    }

}//clase